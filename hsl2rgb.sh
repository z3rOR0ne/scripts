#!/bin/bash

# Convert HSL to RGB
# Usage: hsl_to_rgb $hue $saturation $lightness
function hsl_to_rgb {
  local hue=$1
  local saturation=$2
  local lightness=$3

  local chroma=$(((1 - $((lightness * 2 - 1)) < 0 ? $((lightness * 2 - 1)) * -1 : $((lightness * 2 - 1))) * $saturation))
  local hue_prime=$((hue / 60))
  local x=$((chroma * (1 - $((hue_prime % 2 - 1)) < 0 ? $((hue_prime % 2 - 1)) * -1 : $((hue_prime % 2 - 1)))))
  local r g b

  if ((hue_prime == 0)); then
    r=$chroma
    g=$x
    b=0
  elif ((hue_prime == 1)); then
    r=$x
    g=$chroma
    b=0
  elif ((hue_prime == 2)); then
    r=0
    g=$chroma
    b=$x
  elif ((hue_prime == 3)); then
    r=0
    g=$x
    b=$chroma
  elif ((hue_prime == 4)); then
    r=$x
    g=0
    b=$chroma
  else
    r=$chroma
    g=0
    b=$x
  fi

  local m=$((lightness - chroma / 2))
  r=$((r + m))
  g=$((g + m))
  b=$((b + m))

  # Ensure that the resulting RGB values are within the valid range of 0 to 255
  r=$((r < 0 ? 0 : (r > 255 ? 255 : r)))
  g=$((g < 0 ? 0 : (g > 255 ? 255 : g)))
  b=$((b < 0 ? 0 : (b > 255 ? 255 : b)))

  printf "RGB(%d, %d, %d)\n" $r $g $b
}

# Example usage: convert HSL(180, 100, 50) to RGB
# hsl_to_rgb 180 100 50
hsl_to_rgb "$@"
